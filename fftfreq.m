function [y, mag, phs] = fftfreq(x,w)
% FFT for certain frequencies
%   [y, mag, phs] = fftfreq(x,w)
%
% Inputs:
%   x: input signal as a vector or 2D matrix. If 2D matrix, fft will be 
%      calculated for each column separately. 
%   w: vector of normalized frequency (i.e. between 0 and pi)
% Outputs:
%   y: complex response at the specified frequency
%   mag: response magnitude
%   phs: response phase in radian
% 
% Note that if the response is sin(wt+p)= a sin(wt)+b cos(wt), then we have
% a = real(y) and b = imag(y)
%
%
% Example:
% w = 100 * 2 * pi;     % 100 Hz
% T = 1/1000;           % Sampled at 0.001 sec
% t = [0:1e4-1]*T;      % 10'000 points
% 
% % sinusoidal signal with white noise
% x = 2*sin(w * t + pi/4) + 4 * sin(2*w*t+pi/6) + randn(length(t),1)';
% 
% [y,mag,phs] = fftfreq(x,[1;2]*w*T)
% 
% Results of the example:
% y =
%    1.4075 + 1.4180i
%    3.4664 + 2.0131i
% mag =
%     1.9980
%     4.0086
% phs =
%     0.7891
%     0.5261
if isvector(x)
    x = reshape(x,[],1);
end
[n, nx]= size(x);

nw = length(w);
% x = reshape(x,[],1);
for k = 1:nx
    for i = 1:nw
        tmp = - [0:n-1]' * 1i * w(i) ;   
        y(i,k) = 2 * 1i * sum(exp(tmp).*x(:,k)) / n;
        mag(i,k) = abs(y(i,k));
        phs(i,k) = phase(y(i,k));
    end
end