% Novel directive adaptive control for attenuating periodic disturbances
function MAIN_ADAPT_ZPI()
clear
close all
actuatorType = 'vcm';
rng(0)

%% Design and simulation parameters
Nt = 10e5;                  % number of simulation steps
na = 50;                    % actual model denum order
nah = 5;                    % estimated model denum order 
nbh = 5;                    % estimated model num order
controllerOrder = 7;        % controller order (FIR)
vibrationSigma = 0.10;      % standard deviation of vibration added to the PES
excitationSigma = 2;        % standard deviation of the excitation signal
saturationLimit = 1.25;     % actuator(s) saturation voltage
plotFlag = 1;

lam1 = 1-1e-5;              % forgeting factor for sys ID part
lam2 = 1;
F = eye(nah+nbh+controllerOrder)*10;

lam1c = 1-6e-5;             % forgeting factor for control part
lam2c = 1;
Fc = eye(controllerOrder)*15e5;

%% Sampling time and frequency
Fs = 348*120;  % Sampling Frequency
Ts = 1/Fs;

%% Options for bode plots
bodeOp = bodeoptions();             bodeOp.FreqScale = 'linear';
bodeOp.PhaseWrapping = 'on';        bodeOp.FreqUnits = 'Hz';
bodeOp.XLim = [0.01 1/Ts/2]*1.1;      bodeOp.PhaseMatching = 'on';
bodeOp.PhaseMatchingFreq = (1/Ts)*0.3;   bodeOp.grid = 'on';
bodeOp.PhaseMatchingFreq = 0.05;

%% Load actual system dynamics
if strcmpi(actuatorType, 'ma')
    g = load('ma_1_173');
    sys = tf(g.sys); % full order model for ma
elseif strcmpi(actuatorType, 'vcm')
    g = load('vcm_1_173');
    sys = tf(g.vcmModel); % full order model for vcm (the active actuator)
end
sys.Ts = Ts;
sys = balred(sys,na, 'Elimination', 'Truncate');
% figurename('Sys Bode');
% bode(sys);

% FXIME: make sure that the system is causal
sys = ss(sys);
if sys.d~=0 
    error('System is not strictly causal. The current algorithm does not support this a-causal systems.');
end
sys.d = 0;
sys = canon(sys,'modal');
if ~isstable(sys)
    error('Unstable system')
end

% Project poles of the system to the inside of unit disk IF they are close
% to the stability boundary
eps = 1e-3;
projRadius = 1 - eps;
[z, p, k] = zpkdata(sys,'v');
if any(abs(z)>=1), disp('Just so you know, the closed-loop system is non-minimum phase.'); end

idx = abs(p)>projRadius;
if any(idx)
    error('Some poles of the system are very close to the unit circle. This is not a real error! Change eps above and rerun the code.')
end
p(idx) = p(idx)./abs(p(idx))*projRadius;
sys = zpk(z,p,k,sys.Ts);


%%

[num,den]=tfdata(sys,'v');
num = num/den(1);
den = den/den(1);
As = den(2:end); % -(A^*) in our notation
relDegree = find(num~=0,1) - 1;
% FIXME: This is not correct! We are making the relDegree equal to zero by
% the following line.
A_over_Abar_times_vibration = num(relDegree+1:end);
% B = num;

tA = -As(:);
tB = A_over_Abar_times_vibration(:);

%% Initialize parameters
pA = zeros(na,1);
pAh = zeros(nah,1);

nb = numel(tB);
pB = zeros(nb,1);
pBh = zeros(nbh,1);

pUAdaptive = zeros(nb,1);

tAh = zeros(nah,1);
tBh = zeros(nbh,1);

TA = zeros(size(tAh,1),Nt);
TB = zeros(size(tBh,1),Nt);
U = zeros(Nt,1);
Utot = zeros(Nt,1);
yf = 0;
Y = zeros(Nt,1);
E = zeros(Nt,1);
Eps = zeros(Nt,1);
b1 = zeros(Nt,1);
magAbs = 1;
phsRad = 0;
normTrh = 0.2;
normTR = zeros(Nt,1);
e = 0;

%% Noise models: NRRO and RRO

% Uncomment the following line if you want to create the noise model
% instead of loading it
% tmp = NoiseModel(0);
g = load('noiseModel');
tmp = g.tmp;
rro = tmp.rro;
nrroClosedLoopModel = tmp.noiseModel;
nrro = lsim(nrroClosedLoopModel,randn(Nt,1));

%% Noise models: Vibration
% All frequency values are in Hz.

% Create some narrow band vibrations by peak-filters

% 1st narrow-band content
Fpeak = 4e3;  % Peak Frequency
Q = 20;         % Quality factor
BW =  Fpeak/Q;
Apass = 5;      % Bandwidth Attenuation
[b, a] = iirpeak(Fpeak/(Fs/2), BW/(Fs/2), Apass);
% fvtool(b, a) % uncomment to see the peak filter response
f1 = tf(b,a,-1);
noiseModel = f1;


% 2nd narrow-band content
Fpeak = 3e3;  % Peak Frequency
Q = 20;
BW =  Fpeak/Q;
[b, a] = iirpeak(Fpeak/(Fs/2), BW/(Fs/2), Apass);
% fvtool(b, a)
f1 = tf(b,a,-1);
noiseModel = noiseModel + f1;

% 3rd narrow-band content
Fpeak = 2e3;  % Peak Frequency
Q = 20;
BW =  Fpeak/Q;
Apass = 5;      % Bandwidth Attenuation
[b, a] = iirpeak(Fpeak/(Fs/2), BW/(Fs/2), Apass);
% fvtool(b, a)
f1 = tf(b,a,-1);
noiseModel = noiseModel + f1;

noiseModel.Ts = Ts;
figure('Name','Noise model freq. response');
bode(noiseModel)

vib = lsim(noiseModel, randn(Nt,1)); % Summation of all CLOSED LOOP noises except rro
vib = vib/std(vib)*vibrationSigma;
% FIXME: accel is not neccessarily equal to vibration
accel = vib;

figure('Name','Vibration Spectrum')
hold all
fftp(vib+nrro,Fs,5000)
fftp(vib,Fs,5000)
fftp(nrro,Fs,5000)
legend(['vib+nrro (3\sigma =' num2str(3*std(vib+nrro)*100) '%)'],...
    ['vib (3\sigma =' num2str(3*std(vib)*100) '%)'],...
    ['nrro (3\sigma =' num2str(3*std(nrro)*100) '%)']);


%% Filter for input signal: IIR and BandPass
filtOrder = 8;      % Order
Fpass1 = 500;       % First Passband Frequency
Fpass2 = 15000;     % Second Passband Frequency
Apass  = 1;         % Passband Ripple (dB)
Astop  = 80;        % Stopband Attenuation (dB)
% Construct an FDESIGN object and call its ELLIP method.
h  = fdesign.bandpass('N,Fp1,Fp2,Ast1,Ap,Ast2', filtOrder, Fpass1, Fpass2, ...
                      Astop, Apass, Astop, Fs);
bpFilt = design(h, 'ellip');
[a,b,c,d]=ss(bpFilt);
bpss = ss(a,b,c,d,-1);
% fvtool(bpFilt)

%% Make some copies of the band pass filter for different signals
filterManager = struct();
ufil = bpss; % for the input
yfil = bpss; % for the output
afil = bpss; % for the accelerometer signal
% Prepare the filter structure
filterManager= addstate_LTISIM(filterManager,ufil,zeros(filtOrder,1),'ss');
filterManager= addstate_LTISIM(filterManager,yfil,zeros(filtOrder,1),'ss');
filterManager= addstate_LTISIM(filterManager,afil,zeros(filtOrder,1),'ss');

opt.storeState = 0;
opt.storeOutput = 0;

%% Filter the noises and excitation signal by the band pass filter
% For the identification part, we band-pass the measurements (PES). Since
% the noises are added to the output of the system, they should be filtered
% separately

urand = filter(bpFilt,randn(Nt,1));      % Excitation 
nrro = filter(bpFilt, nrro);             % NRRO
rro = repmat(rro,ceil(Nt/numel(rro)),1); % Make RRO by repeating the profile
rro = rro(1:Nt);
rro = filter(bpFilt, repmat(rro,10,1));  % RRO

if (excitationSigma ~= 0)
    urand = urand/std(urand)*excitationSigma;
else
    urand = urand *0;
end
idx = abs(urand)>saturationLimit;
urand(idx) = sign(urand(idx))*saturationLimit;

%% Construct optimal controller using batch least squares
% The objective of this section is to find the optimal controller assuming
% that we know the exact system dynamics.
preFilt = 1;
A_actual = tf([1 -tA'],[1 zeros(1,na)],-1);
B_actual = tf(tB', [1 zeros(1,nb)],-1);

if preFilt
%     Abar_inverse = balred(inv(Aq),numel(Aq.num{:})-1, 'Elimination', 'Truncate');
    Abar_inverse = inv(A_actual);
    if ~isstable(Abar_inverse), error('Unstable system'); end
    vibInvAbar = lsim(Abar_inverse,vib);
    accelInvAbar = lsim(Abar_inverse,accel);
else
    vibInvAbar = vib;
    accelInvAbar = accel;
end

vibInvAbar_A = lsim(A_actual,vibInvAbar); % A(z)/Abar(z)*vibration
vibActualA = lsim(A_actual, vib);
accelInvAbar_ActualB = lsim(B_actual,accelInvAbar);

% Let's say Ba(k) = B(q^(-1))/Abar(q^-1)*accelerometer(k). The i'th row of X contains
% Ba(i), Ba(i-1), Ba(i-2), ..., Ba(i-controllerOrder+1)
% Assume that the controller is T(1) + T(2)*z^-1 + ... + T(n)*z^(-n) where
% n = controllerOrder. Then, X*T = T(q^-1)[B(q^-1)/Abar(q^-1)[accelerometer(k)]] 
clear X
for i = 1:controllerOrder
    X(:,i) = [zeros(i-1,1); accelInvAbar_ActualB(2:end-i+1,1)];
end
% Now, we want to find the optimal T that minimizes ||T*B/Abar*a-A/Abar*vib||.
% Note that when Abar=A, this is equivalent to minimizing ||B/A*(T*a)-vib||,
% which is equivalent to minimizing the PES because T*a is the adaptive
% control we inject to the system to reduce the vibration

% The optimal controller may have super large coefficients. In order to
% avoid that, we can add a regularization term in the cost of losing a bit
% of performance. The cost function will be the above cost + lambda*||T||
lambda = 0;
B_over_Abar_times_accel = [X; lambda*eye(size(X,2))];
A_over_Abar_times_vibration = vibInvAbar_A(1:end-1,1);
A_over_Abar_times_vibration = [A_over_Abar_times_vibration; zeros(size(B_over_Abar_times_accel,2),size(A_over_Abar_times_vibration,2))];
% Use MATLAB's built in least square solver (mldivide) to find T
T = B_over_Abar_times_accel\A_over_Abar_times_vibration;

% RMS of error
rmsErr = rms(B_over_Abar_times_accel*T-A_over_Abar_times_vibration)

% Optimal controller
Q = tf(T(:).',[1 zeros(1, numel(T)-1)],Ts);

Qa = lsim(Q,accel);
Qab = lsim(Q,accelInvAbar);
BQa = lsim(B_actual,Qab);
QBa = lsim(Q,accelInvAbar_ActualB);

optimalBQab_Aw = (-BQa(2:end)+vibInvAbar_A(1:end-1));
optimalBQa_Aw_Sig = std(optimalBQab_Aw)

yFromCont = lsim(sys,Qa);
yFromCont = yFromCont(2:end);
resid = vib(1:end-1)-yFromCont;

outputExcit = lsim(sys,urand);
optimalTotalErr = outputExcit(1:numel(optimalBQab_Aw)) + resid + nrro(1:end-1);
totalSig = std(optimalTotalErr)

unCompOut = outputExcit(1:numel(optimalBQab_Aw)) + vib(1:end-1) + nrro(1:end-1);

residStd = std(resid)

fprintf('std: vib =  %f\n',std(vib))
fprintf('std: vib-optimalOutput =  %f\n',std(resid))
fprintf('std: nrro =  %f\n',std(nrro))
fprintf('std: vib+nrro =  %f\n',std(vib+nrro))
fprintf('std: vib+nrro-optimalOutput = %f\n',std(resid+nrro(1:end-1)))
fprintf('std: vib+nrro+excitation = %f\n',std(unCompOut))
fprintf('std: vib+nrro+excitation-optimalOutput = %f\n',std(optimalTotalErr))

fprintf('==============================================\n')

figure; fftp(vibInvAbar_A,1)
hold all
fftp(vib,1)
fftp(optimalBQab_Aw,1);
fftp(resid,1);
legend('Aw','w','BQab-Awb','(B/A)Qa-w')
set(gca,'YScale','log')


%% Debug
vibBPlsim = lsim(bpss,vib);
accelBPlsim = lsim(bpss,accel);
accelSys = lsim(sys,accel);
%%
TA = TA*0;
TB = TB*0;

Y = Y*0;
pA = pA*0;
cnt1 = 0;

tRd = zeros(size(T));
TR = zeros(size(tRd,1),Nt);
pVib = zeros(na+1,1);
pAccelInvAbar_Bbar = zeros(size(tRd));
npAccel = max([numel(tRd),numel(tBh),numel(tB)]);
pAccel = zeros(npAccel);
epsilon_hat = 0;


pAccelInvAbar = zeros(npAccel);
pAccelBP = zeros(npAccel);
accelBP = zeros(Nt,1);

tMh = zeros(size(tRd));
TM = zeros(numel(tMh),Nt);

s = struct();
Abar_inverse = tf([1 zeros(1,nah)],[1 -tAh.'],Ts);
s= addstate_LTISIM(s,Abar_inverse,[],'iir');
AhInv4epsilon = tf([1 zeros(1,nah)],[1 -tAh.'],Ts);
s= addstate_LTISIM(s,AhInv4epsilon,[],'iir');


close all

h1 = figure('Name','TA & TB','NumberTitle','off');
h2 = figure('Name','TR','NumberTitle','off');
h3 = figure('Name','std(Y)','NumberTitle','off');
h4 = figure('Name','TM','NumberTitle','off');
h5 = figure('Name','Spectrum: error','NumberTitle','off');
h6 = figure('Name','Bode of estimated system','NumberTitle','off'); 
h7 = figure('Name','Bode of adaptive controller','NumberTitle','off'); 

tic

compOn = 1;
cheatTAH = 0;
cheatTBH = 0;
cheatTMH = 0;
cheatTRD = 0;
nrroOn = 1;
vibOn = 1;



accel = accel* vibOn;
vib = vib* vibOn;

nrro = nrro*nrroOn;





% tRd = -T;

%%
for k = 1:Nt-1
   
    if cheatTRD
        tRd= -T;
    end
    
    if cheatTAH
        tAh = tA;
    end    
    if cheatTBH
        tBh = tB;    
    end
    if cheatTMH
        tMh = [1; -tA];    
    end
    
    pVib = [vib(k); pVib(1:end-1,1)];
    pAccel = [accel(k); pAccel(1:end-1,1)];
    
    s.Abar_inverse.w(1:nah) = -tAh;
    [accelInvAbar(k),s]=updatestate_LTISIM2(s,'Abar_inverse',accel(k),opt);
    
    pAccelInvAbar = [accelInvAbar(k); pAccelInvAbar(1:end-1,1)];
    
    [accelBP(k),filterManager]=updatestate_LTISIM2(filterManager,'afil',accel(k),opt);
    pAccelBP = [accelBP(k); pAccelBP(1:end-1,1)];
    
    debug0 = accelBP(k) - accelBPlsim(k);
    
    accelInvAbar_Bbar(k) = tBh'*pAccelInvAbar(1:numel(tBh),1); 
%     accelInvAbar_Bbar(k) = tB'*pAccelInvAbar(1:numel(tB),1); 
        
    pAccelInvAbar_Bbar = [accelInvAbar_Bbar(k); pAccelInvAbar_Bbar(1:end-1,1)];
    
    
%     accelInvAbar_Bbar-accelSys(k+1)
    
    d = tRd'*pAccel(1:numel(tRd),1);
    debug1 = d+Qa(k); 
    if compOn == false
        d = 0;
    end
    pUAdaptive = [d; pUAdaptive(1:end-1,:)];
    debug2 = BQa(k+1)+tB'*pUAdaptive;
    
    debug3 = vibActualA(k) - [1;-tA(:)]'*pVib;
    % ------ Generate Y ------
    y = tA'*pA + tB'*pB + tB'*pUAdaptive + [1;-tA(:)]'*pVib;

%     r = rro(krot);
    meas = y + nrro(k);%+nrro+r+ otherActuatorOutput(krot);
%     if meas > 2, keyboard; end
    [measf,filterManager]=updatestate_LTISIM2(filterManager,'yfil',meas,opt);

    
    % ------ Update parameters ------
%     yh = tAh'*pAh + tBh'*pBh + tMh'* pAccel(1:numel(tMh),1);
%     yh = tAh'*pAh + tBh'*pBh + tMh'* pAccelBP(1:numel(tMh),1);
    
    theta = [tAh; tBh; tMh];
    phi = [pAh; pBh;  pAccelBP(1:numel(tMh),1)];
    yh = theta'*phi;
    %     if isnan(yh), break; end

    e_apriori = measf - yh;
    
    [theta,F,e_aposteriori] = ...
        LS_update(theta,phi,F,e_apriori,lam1,lam2);
      
    e = e_apriori;

    tAhOld = tAh;
    tAh = theta(1:nah);
    tBh = theta(nah+1:nah+nbh);
    tMh = theta(nah+nbh+1:nah+nbh+numel(tMh));

    if cheatTAH
        tAh = tA;
    else
        if ~mod(k,5)
%             tAh = fstab([1;-tAh],1);
%             tAh = -tAh(2:end,:);
            if any(abs(roots([1;-tAh]))>1-1e-4), tAh = tAhOld; end
        end
    end
    
    if cheatTBH
        tBh = tB;    
    end
    if cheatTMH
        tMh = [1; -tA];    
    end
    
    epsilon_hat = -(meas - (tAh'*pA(1:nah,1) + tBh'*pB(1:nbh,1)));
    s.AhInv4epsilon.w(1:nah) = -tAh;
    [epsilon_hat,s]=updatestate_LTISIM2(s,'AhInv4epsilon',epsilon_hat,opt);
    
        
    idx = max(1,(k-50:k-1)');
    
    gradClippingThreshold = 0;
    e_apriori = epsilon_hat;

    [tRdNew,Fc] = ...
        LS_update(tRd,pAccelInvAbar_Bbar,Fc,e_apriori,lam1c,lam2c,gradClippingThreshold);
    tRd = tRdNew;
    if cheatTRD
        tRd = -T;
    end
    
    dold = d;

    u = urand(k);

    [uf,filterManager]=updatestate_LTISIM2(filterManager,'ufil',u,opt);

    % ------ Update feature vectors ------
    pA = [y;pA(1:end-1,:)]; 
    pAh = [measf;pAh(1:end-1,:)];
    pB = [u; pB(1:end-1,:)];
    pBh = [uf; pBh(1:end-1,:)];
    

% END OF THE ALGORITHM
%==========================================================================
% DATA LOGGING AND VISUALIZATION
%==========================================================================

    Y(k)=meas;
    E(k) = e;
%     U(k) = u;
%     Utot(k) = u+d;
    Eps(k) = epsilon_hat;
    %D(k)=d;

    if ~mod(k,100)
        cnt1 = cnt1+1;
        TR(:,cnt1) = tRd;
        TA(:,cnt1) = tAh;
        TB(:,cnt1) = tBh;        
        TM(:,cnt1) = tMh;
    end
    
    if ~mod(k,ceil(Nt/50))
        figure(h1); 
        plot(TA(:,1:cnt1)','r')
        hold all
        plot(TB(:,1:cnt1)','b')
%         plot([1;size(TA(:,1:cnt1),2)],[tA tA],'r--','linewidth',2)
%         plot([1;size(TA(:,1:cnt1),2)],[tB tB],'b--','linewidth',2)
        hold off
        
        figure(h2)
        plot(TR(:,1:cnt1)')
        hold on
%         plot([1;cnt1],-[T T]','r--','linewidth',2)
        hold off
        
        Ystd = sqrt(movavg(Y(1:k).^2,3000,3000,1));
        figure(h3); 
        semilogy(Ystd(1000:end))
        grid minor
        
        figure(h4)
        plot(TM(:,1:cnt1)')
        hold on
%         plot([1;cnt1],-[T T]','r--','linewidth',2)
        hold off
        
        Qh = tf(tRd.',[1 zeros(1, numel(tRd)-1)],-1);
        yFromCont = lsim(sys*Qh,accel);
        yFromCont = yFromCont(2:end);
        residTmp = vib(1:end-1)+yFromCont;
        fprintf('Attenutation: %f\n',std(residTmp)/std(vib))
        fprintf('output sigma without excitation: %f\n',std(residTmp))
        fprintf('trace(Fc): %f\n',trace(Fc))
        fprintf('---\n');

        figure(h5);
        fftp(vib,Fs,20000);
        hold all
        fftp(residTmp,Fs,20000);
        hold off
        
        sysh = tf([0 tBh' zeros(1,nah-nbh)],[1; -tAh]',1/Fs);
        figure(h6)
        % -- FIG: Bodes -- 
        customizedBode(sys,bodeOp,0,h6.Number);
        customizedBode(sysh,bodeOp,0,h6.Number);
        for i = 1:2
            subplot(2,1,i)
            yl = ylim; 
            ylim(yl);
            xpatch = [Fpass1,Fpass2,Fpass2,Fpass1];
            ypatch = [yl(1) yl(1) yl(2) yl(2)];
            p=patch(xpatch,ypatch,'r','LineStyle','none');
            set(p,'FaceAlpha',0.1);
            hold off
        end
        
        Qh.Ts = Ts;
%         figure(h7);
%         hold all
%         bode(Qh,bodeOp)

        drawnow
    end
    
     if ~mod(k,ceil(Nt/100))
         fprintf('std(Y) =  %f \n',std(Y(k-4000:k)))
        fprintf('std(eps) =  %f \n',std(Eps(k-1000:k)))
        fprintf('std(E) =  %f \n',std(E(k-4000:k)))
        
        
        
     end
     
end
toc



%% Remove zero tail of data
TR = TR(:,1:cnt1);
TA = TA(:,1:cnt1);
TB = TB(:,1:cnt1);
Y = Y(1:k);
Qh = tf(tRd',[1 zeros(1,numel(tRd)-1)],-1);



z = tf('z');

figure('Name','Qh*sys'); 
bode(Qh*sys*z,bodeOp)
legend('Qh*sys*z')
grid minor

figure('Name','Y','NumberTitle','off')
plot(Y)


sysh = tf([0 tBh' zeros(1,nah-nbh)],[1; -tAh]',-1);

sysh.Ts = 1/Fs;
figure(h6)
% -- FIG: Bodes -- 
customizedBode(sys,bodeOp,0,h6);
customizedBode(sysh,bodeOp,0,h6);
for i = 1:2
    subplot(2,1,i)
    yl = ylim; 
    ylim(yl);
    xpatch = [Fpass1,Fpass2,Fpass2,Fpass1];
    ypatch = [yl(1) yl(1) yl(2) yl(2)];
    p=patch(xpatch,ypatch,'r','LineStyle','none');
    set(p,'FaceAlpha',0.1);
end


figure('Name','Pole-Zero maps'); 
iopzmap([sys sysh])


Ystd = sqrt(movavg(Y.^2,2000,2000,1));
figure; 
semilogy(Ystd)
grid minor
% 

figure; 
plot(TA','r')
hold all
plot(TB','b')
plot([1;size(TA,2)],[tA tA],'r--','linewidth',2)
plot([1;size(TA,2)],[tB tB],'b--','linewidth',2)

return
%%
folderName = 't2/';
if isdir(folderName);
    error('Folder exists');
else
    mkdir(folderName);
end
savefig(h1,[folderName,h1.Name])
savefig(h2,[folderName,h2.Name])
savefig(h3,[folderName,h3.Name])
savefig(h4,[folderName,h4.Name])
savefig(h5,[folderName,h5.Name])
savefig(h6,[folderName,h6.Name])

%% Visualize results
tpi = 400e3; 
tpNano = 25.4/tpi*1e6;
mkdir('Figures');
tscale = 1;

% --------- Position Error (nm) -------------
figurename('PES');
plot([1:length(Y)]*tscale,Y*tpNano); 
ylabel('Position Error (nm)','Interpreter','latex');
xlabel('Step','Interpreter','latex');
grid on;
saveImgPdf(6,3,['./Figures/pes-compensated-' num2str(harmCancel(1)),'-',num2str(harmCancel(end)) ]);

% --------- Estimated A and B coefficients -------------
figurename('A,B');
plot(linspace(1,Nt*tscale,size(TA,2)),[-TA' TB'],'linewidth',2);
hold on
for i = 1:size(TA,1)
    legend_str{i} = ['a_',num2str(i)];
end
for i = 1:size(TB,1)
    legend_str{i+size(TA,1)} = ['b_',num2str(i)];
end
legend(legend_str)
xlim([1,Nt*tscale])
ylim([min([-TA(:); TB(:)])-0.1,max(([-TA(:); TB(:)]))+0.1])
xlabel('Step','Interpreter','latex'); 
ylabel('Estimated A and B coefficients','Interpreter','latex');
saveImgPdf(6,3,['./Figures/AB-' num2str(harmCancel(1)),'-',num2str(harmCancel(end)) ]);

% --------- R (\hat{\theta}_M) coefficients -------------
figurename('R');
plot(linspace(1,Nt*tscale,size(TR,2)),TR','linewidth',1);
hold on
grid on
ylabel('$\hat{\theta}_M$ coefficients','Interpreter','latex');
xlabel('Step','Interpreter','latex');
xlim([1,Nt*tscale]);
tmp1 = min(TR(:)); tmp1 = tmp1 * (1-sign(tmp1)*0.1);
tmp2 = max(TR(:)); tmp2 = tmp2 * (1+sign(tmp2)*0.1);
ylim([tmp1 tmp2])
saveImgPdf(6,3,['./Figures/thetaM-' num2str(harmCancel(1)),'-',num2str(harmCancel(end)) ]);

% --------- Estimation Error -------------
time = linspace(1,Nt*tscale,size(E,1));
x = E*tpNano;
figurename('E');
t1 = time(1:end);
x1 = x(1:end);
subplot 121
plot(t1,x1,'linewidth',1);
grid on
ylabel('$\epsilon$: Estimation error (nm)','Interpreter','latex');
xlabel('Step','Interpreter','latex');
xlim([t1(1),t1(end)]);
tmp1 = min(x1); tmp1 = tmp1 * (1-sign(tmp1)*0.1);
tmp2 = max(x1); tmp2 = tmp2 * (1+sign(tmp2)*0.1);
tmp3 = max(-tmp1,tmp2);
ylim(tmp3*[-1 1])

tnmp = 0.3;
t1 = time(end*tnmp:end);
x1 = x(end*tnmp:end);
subplot 122
plot(t1,x1,'linewidth',1);
grid on
% ylabel('$\epsilon$: Estimation error (nm)','Interpreter','latex');
xlabel('Step','Interpreter','latex');
xlim([t1(1),t1(end)]);
tmp1 = min(x1); tmp1 = tmp1 * (1-sign(tmp1)*0.1);
tmp2 = max(x1); tmp2 = tmp2 * (1+sign(tmp2)*0.1);
tmp3 = max(-tmp1,tmp2);
ylim(tmp3*[-1 1])
saveImgPdf(6,3,['./Figures/epsilon-' num2str(harmCancel(1)),'-',num2str(harmCancel(end)) ]);


% ---------- PES spectrum all frequencies ------------
g = load('pes_hgst_raw');
pes = g.pes(1:end/5);
h = figurename('PES and NRRO Spectrum');
plotfft(pes*tpNano,1/period,period*10,h,'g',1.5);
plotfft(Y(end*0.6:end),1/period,period*10,h,'r',1.5);
legend('Adaptive Controller Off', 'Adaptive Controller On', 'Orientation','Horizontal')
box on
ylabel('Amplitude Spectrum (nm)','Interpreter','latex')
xlabel('Harmonic','Interpreter','latex');
saveImgPdf(6,3,['./Figures/spectrum-allfreq-' num2str(harmCancel(1)),'-',num2str(harmCancel(end)) ]);

% ---------- Spectrum only at harmonics ------------
nrev = floor(Nt/period);
n = w(1:nrev*period);
r = repmat(rro, nrev,1);
noi = n + r;
[y, mag, phs] = fftfreq(pes*tpNano, [1:period/2-1]/period*2*pi);
[y2, mag2, phs2] = fftfreq(Y(end*0.6:end)*tpNano, [1:period/2-1]/period*2*pi);
h = figurename('Spectrum - comp and uncomp - bars');
bar([1:period/2-1],mag,'g','EdgeColor','g');
hold on
bar([1:period/2-1],mag2,'r','EdgeColor','r');
ylabel('Amplitude Spectrum (nm)','Interpreter','latex');
xlabel('Harmonic','Interpreter','latex');
grid on; xlim([1,period/2-1]);
legend('Adaptive Controller Off', 'Adaptive Controller On', 'Orientation','Horizontal')
saveImgPdf(6,3,['./Figures/spectrum-bars-',num2str(harmCancel(1)),'-',num2str(harmCancel(end))]);

% ---------- Excitation signal gain ------------
figurename('Norm of tRh');
semilogy(linspace(1,Nt*tscale,size(normTR,1)),normTR,'linewidth',2); 
xlabel('Step','Interpreter','latex')
ylabel('$\alpha_u$: Excitation gain','Interpreter','latex')
grid on;
saveImgPdf(6,3,['./Figures/alphaU-' num2str(harmCancel(1)),'-',num2str(harmCancel(end)) ]);


% ---------- All injenction signals ------------
figurename('Injection Signals');
plot([Utot U]); grid on
legend('Total: excitation + control','Excitation')
xlabel('Step','Interpreter','latex')
ylabel('Injection signals','Interpreter','latex')
saveImgPdf(6,3,['./Figures/injection-' num2str(harmCancel(1)),'-',num2str(harmCancel(end)) ]);


Ainv = tf(1,[1;-tA]',sampleTime);
yuncomp = lsim(sys,randn(Nt,1));
yuncomp = yuncomp + lsim(Ainv,b1);
yuncomp = yuncomp(end/2:end);

ystd = std(Y(end*0.7:end));
fprintf('std(Y) = %f\n',ystd);
ystdMin = norm(sys);
fprintf('min std(Y) = %f\n',ystdMin);
yuncompStd = std(yuncomp);
fprintf('std(Y_uncomp) = %f\n',yuncompStd);


%% Bode options
Ts = 1/period;
sys.Ts = Ts; 
bodeOp = bodeoptions();             bodeOp.FreqScale = 'linear';
bodeOp.PhaseWrapping = 'on';        bodeOp.FreqUnits = 'Hz';
bodeOp.XLim = [0.01 1/Ts/2]*1.1;      bodeOp.PhaseMatching = 'on';
bodeOp.PhaseMatchingFreq = (1/Ts)*0.3;   bodeOp.grid = 'on';

sysh = tf([0 tBh' zeros(1,nah-nbh)],[1; -tAh]',Ts);
% -- FIG: Bodes -- 
h = figurename('Bode of estimated system'); 
customizedBode(sys,bodeOp,0,h);
customizedBode(sysh,bodeOp,0,h);
for i = 1:2
    subplot(2,1,i)
    yl = ylim; 
    ylim(yl);
    xpatch = [harmCancel(1),harmCancel(end),harmCancel(end),harmCancel(1)];
    ypatch = [yl(1) yl(1) yl(2) yl(2)];
    p=patch(xpatch,ypatch,'r','LineStyle','none');
    set(p,'FaceAlpha',0.1);
end
saveImgPdf(6,3,['./Figures/bode-',num2str(harmCancel(1)),'-',num2str(harmCancel(end))]);

% -- FIG: Diff of bodes -- 
h = figurename('Diff in Bode of estimated system'); 
customizedBode(sys/sysh,bodeOp,0,h);
for i = 1:2
    subplot(2,1,i)
    yl = ylim; 
    ylim(yl);
    xpatch = [harmCancel(1),harmCancel(end),harmCancel(end),harmCancel(1)];
    ypatch = [yl(1) yl(1) yl(2) yl(2)];
    p=patch(xpatch,ypatch,'r','LineStyle','none');
    set(p,'FaceAlpha',0.1);
end
saveImgPdf(6,3,['./Figures/bode-diff-',num2str(harmCancel(1)),'-',num2str(harmCancel(end))]);


fprintf('Stability: %i\n',isstable(sysh));


%% Output argument
out.uffwd = uffwd;
% keyboard