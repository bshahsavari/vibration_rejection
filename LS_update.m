function [theta,F,e_aposteriori] = LS_update(theta,phi,F,e_apriori,lam1,lam2,gradClippingThreshold)
Fphi = F * phi;
e_aposteriori = lam1*e_apriori/(lam1+phi'*Fphi);
update = 1/lam1*F*phi*e_aposteriori;
% idx = abs(update)<gradClippingThreshold;
% update(idx) = 0;
theta = theta + update;
F = 1/lam1*(F-lam2*(Fphi*Fphi')/(lam1+lam2*phi'*Fphi) );
    