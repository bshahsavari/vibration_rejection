function saveImgPdf(x,y,name,scale)
% figure; plot(randn(100,1));
% saveImgPdf(6,3,'example.pdf')
if ~exist('scale','var'), scale = 1; end
set(gcf, 'PaperPosition', [x/2*(1-scale) y/2*(1-scale) x/2*(1+scale) y/2*(1+scale)]); %Position plot at the center with length x*scale and heigh y*scale
set(gcf, 'PaperSize', [x y]); %Set the paper to have width and height a little bit smaller than x and y 
saveas(gcf, name, 'pdf') %Save figure
saveas(gcf, name, 'fig') %Save figure
